# benchmark-cloud-fr

Lister les fonctionnalités et leurs disponibilités chez les clouds FR

## fonctionnalités

### Global

|  | OVHcloud | Scaleway | Outscale |  NumSpot | AWS      |
| - | -       | -        | -        | -            | -            |
| Aggregation des logs |
| Gestion d'identité | | ✅ [beta](https://www.scaleway.com/fr/betas/#iam-identity-and-access-management) | | |
| Gestion des droits |
| ↳ + avec des rôles |
| Métrologie |
| SSO |
| Shell en ligne |
| Support |
| Étiquetage (Tags) |
| Protection des objets |
| Audit |

### Calcul

|  | OVHcloud | Scaleway | Outscale | NumSpot |
| - | -       | -        | -        | -            |
| Architecture ARM |
| GPU |
| Accélérateur d'IA (NPU) |
| Instances éphémères (spot) |

### Réseau

|  | OVHcloud | Scaleway | Outscale |  NumSpot |
| - | -       | -        | -        | -            |
| Réseau Privé |
| Appairage |
| Filtrage |
| ↳ + avec étiquetage |
| Point de terminaison privé |
| Passerelle NAT |
| Repartiteur de Charge |
| ↳ + privé |
| Hub VPN |
| Export des flow |
| IPv6 |
| Zone de disponibilité |

### Serverless

|  | OVHcloud | Scaleway | Outscale |  NumSpot |
| - | -       | -        | -        | -            |
| Framework de dev |
| Runtime |
| ↳ + nodejs
| ↳ + python
| ↳ + custom
| Accès au VPC |
| Assume un rôle |
| Invocation sur un évenement |

### Stockage

|  | OVHcloud | Scaleway | Outscale |  NumSpot |
| - | -       | -        | -        | -            |
| local persistant |
| partagé |
| snapshot |

### Stockage Objet

|  | OVHcloud | Scaleway | Outscale |  NumSpot |
| - | -       | -        | -        | -            |
| Objet S3 compatible |
| Life-cycle |
| Archive |

### Database

|  | OVHcloud | Scaleway | Outscale |  NumSpot |
| - | -       | -        | -        | -            |
| Relational (MySQL, Postgres) |
| NoSQL (MongoDB, Cassandra) |
| Datawarehouse  |
| ETL |
| Evenements (pubsub) |
| Time Series |

### Kubernetes

|  | OVHcloud | Scaleway | Outscale | NumSpot  |
| - | -       | -        | -        | -            |
| Sans noeud |
| API privée |
| Dimensionnement Auto |
| Concentration des logs |
| Metrologie intégrée |
| OS managé |
| ↳ + container native |
| Gestion des ressources par CRD |

## Outillage

|  | OVHcloud | Scaleway | Outscale |  NumSpot |
| - | -       | -        | -        | -            |
| CLI |
| Terraform provider |
| Ansible |
| Crossplane |
| Pulumi |

## Compliance

|  | OVHcloud | Scaleway | Outscale | NumSpot |
| - | -       | -        | -        | -            |
| ISO 27001 | | ✅ 
| HDS | ✅ [1,2,3,4] |  | ✅ [1,2,3,4] | 
| SecNumCloud |✅ [hosted private cloud] |  | ✅ [cloudgouv]
| PCI-DSS |
| SOC1/2 |
